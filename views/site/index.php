<?php

/** @var yii\web\View $this */
use yii\grid\GridView;
$this->title = 'Examen Pablo Saturio Herrezuelo';
?>
<div class="site-index">
 
    <div class="jumbotron bg-transparent text-center">
        <h1>1. Ciclistas cuya edad está entre 25 y 35 años</h1> 
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider1,
        'tableOptions' => [
            'class'=>'table thead-dark table-hover table-bordered text-center table-sm'
        ],
        'columns' => [
            'dorsal',
            'nombre',
        ],
    ]) ?>
    
    <div class="jumbotron bg-transparent text-center">
        <h1>2. Etapas no circulares</h1> 
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider2,
        'tableOptions' => [
           'class'=>'table thead-dark table-hover table-bordered text-center table-sm'
        ],
        'columns' => [
            'numetapa',
            'kms',
        ],
    ]) ?>
    
    <div class="jumbotron bg-transparent text-center">
        <h1>3. Puertos con altura mayor a 1500m</h1> 
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider3,
        'tableOptions' => [
            'class'=>'table thead-dark table-hover table-bordered text-center table-sm'
        ],
        'columns' => [
            'nompuerto',
            'dorsal',
        ],
    ]) ?>
    
</div>
